# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/choose_t.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/choose_t.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/consts.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/consts.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_freeze.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_freeze.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_mul.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_mul.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_nsquare.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_nsquare.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_square.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_square.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_add_p1p1.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_add_p1p1.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_dbl_p1p1.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_dbl_p1p1.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_nielsadd2.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_nielsadd2.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_nielsadd_p1p1.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_nielsadd_p1p1.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_p1p1_to_p2.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_p1p1_to_p2.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_p1p1_to_p3.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_p1p1_to_p3.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_p1p1_to_pniels.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_p1p1_to_pniels.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_pnielsadd_p1p1.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_pnielsadd_p1p1.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced_1limb.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced_1limb.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced_2limbs.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced_2limbs.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced_3limbs.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/heap_rootreplaced_3limbs.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_add.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_add.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_barrett.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_barrett.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_lt.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_lt.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_sub_nored.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_sub_nored.s.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ull4_mul.s" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ull4_mul.s.o"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "BINARY_OUTPUT"
  "BN_SUPPORT_SNARK=1"
  "CURVE_BN128"
  "MONTGOMERY_OUTPUT"
  "USE_ASM"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "/usr/include"
  "."
  "depends/ate-pairing/include"
  "depends/xbyak"
  "depends/libsnark-supercop/include"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_core/aes128encrypt/openssl/core.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_core/aes128encrypt/openssl/core.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_hash/sha256/sphlib/hash.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_hash/sha256/sphlib/hash.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_hash/sha256/sphlib/hash_sha256.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_hash/sha256/sphlib/hash_sha256.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_hash/sha256/sphlib/sha2.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_hash/sha256/sphlib/sha2.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_hash/sha512/openssl/hash.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_hash/sha512/openssl/hash.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/batch.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/batch.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_add.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_add.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_getparity.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_getparity.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_invert.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_invert.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_iseq.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_iseq.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_iszero.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_iszero.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_neg.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_neg.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_pack.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_pack.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_pow2523.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_pow2523.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_setint.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_setint.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_sub.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_sub.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_unpack.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/fe25519_unpack.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_add.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_add.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_base.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_base.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_double.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_double.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_double_scalarmult.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_double_scalarmult.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_isneutral.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_isneutral.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_multi_scalarmult.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_multi_scalarmult.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_pack.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_pack.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_scalarmult_base.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_scalarmult_base.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_unpackneg.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/ge25519_unpackneg.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/hram.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/hram.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/index_heap.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/index_heap.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/keypair.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/keypair.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/open.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/open.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_from32bytes.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_from32bytes.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_from64bytes.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_from64bytes.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_from_shortsc.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_from_shortsc.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_iszero.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_iszero.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_mul.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_mul.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_mul_shortsc.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_mul_shortsc.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_slide.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_slide.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_to32bytes.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_to32bytes.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_window4.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sc25519_window4.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sign.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_sign/ed25519/amd64-51-30k/sign.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_verify/16/ref/verify.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_verify/16/ref/verify.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_verify/32/ref/verify_32.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_verify/32/ref/verify_32.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/crypto_verify/8/ref/verify.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/crypto_verify/8/ref/verify.c.o"
  "/root/libsnark-docker/depends/libsnark-supercop/src/randombytes.c" "/root/libsnark-docker/depends/CMakeFiles/snark_supercop.dir/libsnark-supercop/src/randombytes.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BINARY_OUTPUT"
  "BN_SUPPORT_SNARK=1"
  "CURVE_BN128"
  "MONTGOMERY_OUTPUT"
  "USE_ASM"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "depends/ate-pairing/include"
  "depends/xbyak"
  "depends/libsnark-supercop/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
