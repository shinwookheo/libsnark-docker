# CMake generated Testfile for 
# Source directory: /root/libsnark-docker/depends
# Build directory: /root/libsnark-docker/depends
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(libff)
subdirs(libfqfft)
